#!/bin/bash

function setup()
{
	chmod +x src/*.sh
}

function update()
{
	# Update and upgrade all packages
	sudo apt-get update -y
	sudo apt-get upgrade -y
}

function install()
{
	# Install all dependencies
	sudo apt-get install wget git -y

	# Install tmux
	sudo apt-get install tmux -y

	# Install zsh and oh-my-zsh
	src/install_zsh.sh

}

function files()
{
	# Copy the needed files
	cp src/inputrc $HOME/.inputrc
	cp src/vimrc $HOME/.vimrc
}

function usage()
{
	echo "Usage:"
	echo "	[-a] install all"
	echo "	[-h] print this message"
	echo "	[-f] install powerline-font"
}

function fonts()
{
	src/powerline_font.sh
}

setup

while getopts "?afh" OPTION; do
	case $OPTION in
		a)
			update
			install
			files
			fonts
			exit
			;;
		f)
			fonts
			exit
			;;
		h)
			usage
			exit
			;;
		?)
			usage
			exit
			;;
	esac
done

if [ $OPTIND -eq 1 ]; then
	usage
	exit
fi
