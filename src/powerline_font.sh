#!/usr/bin/env bash

# Fetch the symbols
wget https://github.com/powerline/powerline/raw/develop/font/PowerlineSymbols.otf
wget https://github.com/powerline/powerline/raw/develop/font/10-powerline-symbols.conf

# Install fonts
mkdir ~/.fonts
mv PowerlineSymbols.otf ~/.fonts/
sudo fc-cache -vf ~/.fonts

mkdir -p ~/.config/fontconfig/conf.d
mv 10-powerline-symbols.conf ~/.config/fontconfig/conf.d/
