#!/usr/bin/env bash

# Install Zsh and wget
sudo apt-get install zsh -y

# Install Oh-my-zsh
sh -c "$(wget https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)"
