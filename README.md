# Basic Setup scripts

## Environments with root access
- inputrc for history searches
- vimrc for 4-space tab, mouse and number of lines
- installation of zsh and oh-my-zsh
- installation of powerline-fonts (use -f to install)

## Environments without root access
